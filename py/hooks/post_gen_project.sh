#!/bin/sh

set -e

git init

git config commit.template .git-commit-template

HOOKS={{cookiecutter.use_hooks}}

if [ "$HOOKS" = "yes" ]
then
    git config core.hooksPath dev-bin/git-hooks
fi

git add -A
git commit --no-verify \
    -m "Initial import of python-{{cookiecutter.repo_name}}"

exit 0
