import os

from flask import Blueprint, safe_join, send_from_directory

docs = Blueprint("docs", __name__)


@docs.route("/_modules/<path:module_path>")
def doc_module(module_path):
    """Endpoint for retrieving styled source files

    Maps `/_module/<module_path>` URLs to `_modules` in the `docs` root

    """
    path_parts = module_path.split("/")[:-1]
    module = module_path.split("/")[-1:][0]
    module_root = safe_join("docs/_modules", *path_parts)
    return send_from_directory(os.path.abspath(module_root), module)


@docs.route("/_static/<asset>")
def doc_asset(asset):
    """Endpoint for retrieving static documentation resources

    Maps `/_static/<asset>` URLs to `_static` in the `docs` root

    """
    return send_from_directory(os.path.abspath("docs/_static"), asset)


@docs.route("/", methods=["GET"])
@docs.route("/<page>", methods=["GET"])
def doc_page(page="index.html"):
    """Main documentation endpoint for the documentation

    Maps `/[<page>]` URLs to the `docs` root

    """
    return send_from_directory(os.path.abspath("docs"), page)
