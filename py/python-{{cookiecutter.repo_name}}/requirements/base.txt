black
python-decouple
flake8
isort
sphinx
{%- if cookiecutter.type == 'http' %}
# insert http specifics
{%- elif cookiecutter.type == 'rmq' %}
# insert rmq specifics
{%- elif cookiecutter.type == 'cli' %}
# insert cli specifics
{%- endif %}
