# Welcome to {{cookiecutter.repo_name}}

{{cookiecutter.description}}

## gitignore

Please educate yourself and use a global gitignore file. Somes files will
always be there. I'm looking at you `node_modules`.

### The lockfiles
You'll also see that `package-lock.json` and/or `yarn.lock` is ignored. I
strongly believe that pinning packages lead to bit rot, as you never update
packages. When a downstream module breaks it API you should now and update your
code accordingly. The same goes for possible upstream modules. If you want a
specific version, pin it to that version, otherwise use the latest and greatest
when possible.
