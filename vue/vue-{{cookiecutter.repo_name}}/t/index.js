require('esm');

import tap from "tap";
import foo from '../lib/index.js';

tap.type(foo, 'function',
  "foo is exported by index.js")

tap.is(foo('something'), true,
  "foo returns true")
