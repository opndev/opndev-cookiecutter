import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: false,
    headers: {
        accept: 'application/json',
        'content-type': 'application/json',
    },
});

export default {
}
