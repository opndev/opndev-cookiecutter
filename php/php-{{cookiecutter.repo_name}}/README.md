# {{cookiecutter.repo_name}}

## Getting started

n/a

## Contributing

Please read
[CONTRIBUTING.md](https://gitlab.com/opndev/{{cookiecutter.repo_name}}/blob/master/CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting pull
requests to us.
